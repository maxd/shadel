#include <FastLED.h>
#include <Shadel.h>

// How many leds in your strip?
#define NUM_LEDS 150
#define DATA_PIN 2

// Define the array of leds
CRGB leds[NUM_LEDS];

// define the pixel buffer
// pixels hold a xy coordinates, a color, and a LED address
ShadelPixel pixels[NUM_LEDS];

#define STRIP_COUNT 4
PixelStrip strips[STRIP_COUNT];

const unsigned int stripAddresses[STRIP_COUNT] = { 
    0, 11, 22, 33
};

const unsigned int stripLengths[STRIP_COUNT] = {
    11, 11, 11, 11
};

void setup() { 
    
    FastLED.addLeds<WS2812B, DATA_PIN, RGB>(leds, NUM_LEDS);
    
    // shadelPrecalc fills the sin look up table
    shadelPrecalc();
    for(int i = 0; i < STRIP_COUNT; i++){
        // first we need to bind pixels to the strip's pixelgroup
        strips[i].pg.bindPixels(
            // pointer to a spot int the pixels array
            pixels+stripAddresses[i],
            // how many LEDs
            stripLengths[i],
            // FastLED leds[] address
            stripAddresses[i],
            // max number of LEDs
            NUM_LEDS
        );
        // then we can map the LEDs
        float y = i/float(STRIP_COUNT);
        // map the leds along the X axis from 0.0 to 1.0
        // each strip spaced verticaly
        Vec2 pointA = Vec2(0.0, y);
        Vec2 pointB = Vec2(1.0, y);
        strips[i].map(pointA, pointB);
    }
}


// lets make some shaders
// we at least need a time uniform
float ttime = 0;
CRGB simpleShader(Vec2 _pos, CRGB _c){
    float f = sinLook(_pos.x*PI+_pos.y+ttime*.5)*.5+.5;
    f = f*f;
    f = smoothstep(0.3, 0.7, f);
    return blend(CRGB::Black, _c, f*255); 
}

CRGB otherShader(Vec2 _pos, CRGB _c){
    float f = sinLook(_pos.y*.4+ttime)*.5+.5;
    f = f*.8+.2;
    return blend(CRGB::Black, _c, f*255);
}


void loop() { 
    // set the uniform value
    ttime = millis()/1000.0;
    // process each strip!
    for(int i = 0; i < STRIP_COUNT; i++){
        // set a color for the strip
        strips[i].pg.setColor(CHSV(i*42, 255, 255));
        // apply a shader!
        strips[i].pg.applyShader(simpleShader);
        // maybe an other!
        // strips[i].pg.applyShader(otherShader);
        
        // then copy out the data to the FastLED CRGB array
        strips[i].pg.applyToLeds(leds);
    }
    FastLED.show();
}