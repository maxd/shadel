#ifndef SHADEL_LUTS_H
#define SHADEL_LUTS_H

#include "Arduino.h"

#ifndef PI
    #define PI          3.1415927
#endif
#ifndef HALF_PI
    #define HALF_PI     PI/2.0
#endif
#ifndef QUARTER_PI
    #define QUARTER_PI  PI/4.0
#endif
#ifndef TWO_PI
    #define TWO_PI      PI*2.0
#endif

#define SHADEL_USE_LOOKUP 1

#define SHADEL_LOOKUP_TABLESIZE 1024
// lookup table for sin
void shadelPrecalc();
float sinLook(float f);
float cosLook(float f);

#endif // SHADEL_LUTS_H
