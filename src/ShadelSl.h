#ifndef SHADEL_SL_H
#define SHADEL_SL_H

#include "Arduino.h"
#include "ShadelLuts.h"
// Shadel Shading Language

// These are functions used to program animations like a frag shader
struct Vec2 {
    float x;
    float y;
    Vec2();
    Vec2(float _x, float _y);

    Vec2 lerp(const Vec2& v, float u);
    Vec2 operator +(const Vec2& v);
    Vec2 operator -(const Vec2& v);
    Vec2 operator *(const Vec2& v);
    Vec2 operator /(const Vec2& v);

    Vec2& operator +=(const Vec2& v);
    Vec2& operator -=(const Vec2& v);
    Vec2& operator *=(const Vec2& v);
    Vec2& operator /=(const Vec2& v);

    Vec2 operator +(float f);
    Vec2 operator -(float f);
    Vec2 operator *(float f);
    Vec2 operator /(float f);

    Vec2& operator +=(float f);
    Vec2& operator -=(float f);
    Vec2& operator *=(float f);
    Vec2& operator /=(float f);
};

struct Vec3 {
    float x;
    float y;
    float z;

    Vec3();
    Vec3(float _x, float _y, float _z);

    Vec3 lerp(const Vec3& v, float u);
    Vec3 operator +(const Vec3& v);
    Vec3 operator -(const Vec3& v);
    Vec3 operator *(const Vec3& v);
    Vec3 operator /(const Vec3& v);

    Vec3& operator +=(const Vec3& v);
    Vec3& operator -=(const Vec3& v);
    Vec3& operator *=(const Vec3& v);
    Vec3& operator /=(const Vec3& v);

    Vec3 operator +(float f);
    Vec3 operator -(float f);
    Vec3 operator *(float f);
    Vec3 operator /(float f);

    Vec3& operator +=(float f);
    Vec3& operator -=(float f);
    Vec3& operator *=(float f);
    Vec3& operator /=(float f);
};



// glsl implementations
float clamp(float min, float max, float x);
Vec3 clamp(Vec3 x, float min, float max);

float smoothstep(float a, float b, float x);
Vec2 smoothstep(float a, float b, Vec2 p);

float fract(float f);
Vec2 fract(Vec2 f);
Vec2 floor(Vec2 f);
float dist(Vec2 a, Vec2 b);
float dot(Vec2 a, Vec2 b);
float mix(float a, float b, float l);
Vec2 rotate(Vec2 v, float a);

//
float floatmod(float a, float b);

#endif // SHADEL_SL_H
