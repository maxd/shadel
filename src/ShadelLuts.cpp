#include "ShadelLuts.h"

float sinTable[SHADEL_LOOKUP_TABLESIZE];

void shadelPrecalc(){
    for(int i = 0; i < SHADEL_LOOKUP_TABLESIZE; i++) {
        sinTable[i] = sin(float(i)/float(SHADEL_LOOKUP_TABLESIZE)*TWO_PI);
    }
}

float sinLook(float f){
    f = fmod(f, PI);
    uint16_t idx = uint16_t(f/PI*SHADEL_LOOKUP_TABLESIZE);
    return sinTable[idx];
}
