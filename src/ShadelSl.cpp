#include "ShadelSl.h"


// Vec2::Vec2(float x=0, float y=0) : x(x), y(y)
// {}
Vec2::Vec2(){x =.0; y = .0;}

Vec2::Vec2(float _x, float _y){x = _x; y = _y;}


Vec2 Vec2::lerp(const Vec2& v, float u){
    return Vec2( x+u*(v.x-x), y+u*(v.y-y));
}

Vec2 Vec2::operator +(const Vec2& v){
    return Vec2(x+v.x, y+v.y);
}
Vec2 Vec2::operator -(const Vec2& v){
    return Vec2(x-v.x, y-v.y);
}
Vec2 Vec2::operator *(const Vec2& v){
    return Vec2(x*v.x, y*v.y);
}
Vec2 Vec2::operator /(const Vec2& v){
    return Vec2(x/v.x, y/v.y);
}

Vec2& Vec2::operator +=(const Vec2& v){
    x += v.x;
    y += v.y;
    return *this;
}
Vec2& Vec2::operator -=(const Vec2& v){
    x -= v.x;
    y -= v.y;
    return *this;
}
Vec2& Vec2::operator *=(const Vec2& v){
    x *= v.x;
    y *= v.y;
    return *this;
}
Vec2& Vec2::operator /=(const Vec2& v){
    x /= v.x;
    y /= v.y;
    return *this;
}


Vec2 Vec2::operator +(float f){
    return Vec2(x+f, y+f);
}
Vec2 Vec2::operator -(float f){
    return Vec2(x-f, y-f);
}
Vec2 Vec2::operator *(float f){
    return Vec2(x*f, y*f);
}
Vec2 Vec2::operator /(float f){
    return Vec2(x/f, y/f);
}

Vec2& Vec2::operator +=(float f){
    x += f;
    y += f;
    return *this;
}
Vec2& Vec2::operator -=(float f){
    x -= f;
    y -= f;
    return *this;
}
Vec2& Vec2::operator *=(float f){
    x *= f;
    y *= f;
    return *this;
}
Vec2& Vec2::operator /=(float f){
    x /= f;
    y /= f;
    return *this;
}


//////////////////////////////////////////////////////////////////////////////

// Vec2::Vec2(float x=0, float y=0) : x(x), y(y)
// {}
Vec3::Vec3(){x = .0; y = .0; z = .0;}

Vec3::Vec3(float _x, float _y, float _z){x = _x; y = _y; z = _z;}


Vec3 Vec3::lerp(const Vec3& v, float u){
    return Vec3( x+u*(v.x-x), y+u*(v.y-y), z+u*(v.z-z));
}

Vec3 Vec3::operator +(const Vec3& v){
    return Vec3(x+v.x, y+v.y, z+v.z);
}
Vec3 Vec3::operator -(const Vec3& v){
    return Vec3(x-v.x, y-v.y, z-v.z);
}
Vec3 Vec3::operator *(const Vec3& v){
    return Vec3(x*v.x, y*v.y, z*v.z);
}
Vec3 Vec3::operator /(const Vec3& v){
    return Vec3(x/v.x, y/v.y, z/v.z);
}

Vec3& Vec3::operator +=(const Vec3& v){
    x += v.x;
    y += v.y;
    z += v.z;
    return *this;
}
Vec3& Vec3::operator -=(const Vec3& v){
    x -= v.x;
    y -= v.y;
    z -= v.z;
    return *this;
}
Vec3& Vec3::operator *=(const Vec3& v){
    x *= v.x;
    y *= v.y;
    z *= v.z;
    return *this;
}
Vec3& Vec3::operator /=(const Vec3& v){
    x /= v.x;
    y /= v.y;
    z /= v.z;
    return *this;
}


Vec3 Vec3::operator +(float f){
    return Vec3(x+f, y+f, z+f);
}
Vec3 Vec3::operator -(float f){
    return Vec3(x-f, y-f, z-f);
}
Vec3 Vec3::operator *(float f){
    return Vec3(x*f, y*f, z*f);
}
Vec3 Vec3::operator /(float f){
    return Vec3(x/f, y/f, z/f);
}

Vec3& Vec3::operator +=(float f){
    x += f;
    y += f;
    z += f;
    return *this;
}
Vec3& Vec3::operator -=(float f){
    x -= f;
    y -= f;
    z -= f;
    return *this;
}
Vec3& Vec3::operator *=(float f){
    x *= f;
    y *= f;
    z *= f;
    return *this;
}
Vec3& Vec3::operator /=(float f){
    x /= f;
    y /= f;
    z /= f;
    return *this;
}





float clamp(float min, float max, float x){
    if (x < min) {
        x = min;
    }
    else if (x > max) {
        x = max;
    }
    return x;
}

Vec3 clamp(Vec3 v, float min, float max){
    return Vec3(clamp(min,max,v.x), clamp(min,max,v.y), clamp(min,max,v.z));
}

float smoothstep(float a, float b, float x) {
    // Scale, bias and saturate x to 0..1 range
    x = clamp(0.0, 1.0, (x - a) / (b - a));
    // Evaluate polynomial
    return x * x * (3.0 - 2.0 * x);
}

Vec2 smoothstep(float a, float b, Vec2 p){
    return Vec2(smoothstep(a,b,p.x),smoothstep(a,b,p.y));
}

float fract(float f) {
    return f-floor(f);
}

Vec2 fract(Vec2 v){
    return Vec2(fract(v.x), fract(v.y));
}

Vec2 floor(Vec2 v){
    return Vec2(floor(v.x), floor(v.y));
}

float dist(Vec2 a, Vec2 b) {
    return sqrt(pow(a.x-b.x, 2)+pow(a.y-b.y, 2));
}

float dot(Vec2 a, Vec2 b){
    return a.x*b.x+a.y*b.y;
}

float mix(float a, float b, float l){
    return a+l*(b-a);
}

float floatmod(float a, float b) {
    return a-floor(a/b)*b;
}

Vec2 rotate(Vec2 v, float a) {
    float co = sinLook(a+HALF_PI);
    float si = sinLook(a);
    Vec2 out = {
        v.x * co - v.y * si,
        v.x * si + v.y * co
    };
    return out;
}
