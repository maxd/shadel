#include "ShadelPixel.h"

ShadelPixel::ShadelPixel(){

}

void ShadelPixel::applyToLeds(CRGB * _leds){
    // potentialy problematic
    _leds[ledAddress] = c;
}

// bindPixels
// attach pixel array to pixelgroup, set CRGB[] array address.
void PixelGroup::bindPixels(ShadelPixel * p, uint16_t s, uint16_t _a, uint16_t _maxleds){
    size = s;
    pixels = p;
    groupLEDAddress = _a;
    // clamp the size to the max_led_count
    if(s+groupLEDAddress > _maxleds) size = _maxleds - groupLEDAddress;
}

void PixelGroup::applyShader(CRGB (*shade)(Vec2, CRGB)){
    for(int i = 0; i < size; i++){
        pixels[i].c = shade(pixels[i].pos, pixels[i].c);
    }
}


void PixelGroup::applyShader(CRGB (*shade)(Vec2, CRGB, int, int)){
    for(int i = 0; i < size; i++){
        pixels[i].c = shade(pixels[i].pos, pixels[i].c, i, pixels[i].ledAddress);
    }
}


// add more of these with blending strategies
void PixelGroup::applyToLeds(CRGB* _leds){
    for(int i = 0; i < size; i++){
        pixels[i].applyToLeds(_leds);
    }
}

void PixelGroup::setColor(CRGB c){
    for(int i = 0; i < size; i++){
        pixels[i].c = c;
    }
}


void PixelMatrix::map(
        uint16_t _w,
        uint16_t _h,
        Vec2 _pos,
        uint8_t _mapStyle
    ){
    width = _w;
    height = _h;
    uint16_t address = 0;
    uint16_t idxTexture = 0;
    for(int hh = 0; hh < height; hh++){
        for(int ww = 0; ww < width; ww++){
            address = hh*width;
            if(_mapStyle == MAP_ZIGZAG){
                address += (hh%2==1) ? width-1-ww : ww;
            }
            else {
                address += ww;
            }
            if(address < pg.size){
                // idxTexture = hh*width+ww;
                idxTexture = ww*height+hh;
                pg.pixels[idxTexture].ledAddress = address+pg.groupLEDAddress;
                pg.pixels[idxTexture].pos =
                    Vec2(
                        ww/float(height),
                        hh/float(height ) // aspect?

                        // hh/float(_mapStyle == MAP_ASPECT ? width : height) // aspect?
                    )+_pos;
                //
            }
        }
    }
}

void PixelMatrix::setPixel(uint16_t x, uint16_t y, CRGB c){
    uint16_t idx = x*height+y;
    if(idx < pg.size){
        pg.pixels[idx].c = c;
    }
}

void PixelMatrix::addToPixel(uint16_t x, uint16_t y, CRGB c){
    uint16_t idx = x*height+y;
    if(idx < pg.size){
        pg.pixels[idx].c += c;
    }
}

CRGB PixelMatrix::getPixel(uint16_t x, uint16_t y){
    uint16_t idx = x*height+y;
    if(idx < pg.size){
        return pg.pixels[idx].c;
    }
    else {
        return pg.pixels[0].c;
    }
}


void PixelStrip::map(Vec2 pa, Vec2 pb){
    // interpolate
    float interval = 1./float(pg.size);
    for(uint16_t i = 0; i < pg.size; i++){
        pg.pixels[i].ledAddress = i+pg.groupLEDAddress;
        pg.pixels[i].pos = pa.lerp(pb, i*interval);
    }
}


// Vec2 rotate(Vec2 v, float a) {
//     float co = sinLook(a+HALF_PI);
//     float si = sinLook(a);
//     Vec2 out = {
//         v.x * co - v.y * si,
//         v.x * si + v.y * co
//     };
//     return out;
// }

// Vec2 angleMapHelp(){
//     // float co = sinLook(a+HALF_PI);
//     // float si = sinLook(a);
//     // Vec2 out = {
//     //     v.x * co - v.y * si,
//     //     v.x * si + v.y * co
//     // };
//     // return out;
// }

// pa = center location
// pb.x = angle range
// pb.y = radius? 1.0?  
// void PixelStrip::mapRing(Vec2 center, Vec2 angleRadius){
    // interpolate
    // float interval = 1./float(pg.size);
    // for(uint16_t i = 0; i < pg.size; i++){
    //     pg.pixels[i].ledAddress = i+pg.groupLEDAddress;
    //     float angle = i*interval*PI;
    //     float radius = 
    //     // pg.pixels[i].pos = pa.lerp(pb,);
    // }
// }
