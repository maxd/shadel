### Shadel
fragment shader style programming for FastLED

#### Types
`Vec2` 2D vector, support operators (+,-, \*, /, and +=, -=, \*=, /=) with an other Vec2 or a float.

`Pixel` a ledAddress with a Vec2 and a CRGB color. Declare sized arrays for different elements, during setup bind the to a texture or map them to a fixture.

`PixelGroup` holds a pointer to a sized pixel array, iterates over pixels to set color or apply shaders or draw to screen.

<!-- ```
#define NUM_LEDS 512
CRGB leds[NUM_LEDS];
#define ELEMENT_PIXEL_COUNT 64
Pixel elementPixels[ELEMENT_PIXEL_COUNT];
PixelGroup pixelGroup;

void setup(){
    elementTexture.bindPixels(elementPixels, ELEMENT_PIXEL_COUNT);
}     

CRGB effectA(Vec2 pos, CRGB c){
    // random dimming of all
    c/=random(10)+1;
    pos*=Vec2(10.,3.);
    float f = sin(pos.x*pos.y)+1.0;
    c.b = f*200;
    return c;
}

void loop(){
    elementTexture.setColor(CRGB(255,0,0));
    elementTexture.applyShader(effectA);
    elementTexture.drawToScreen(fastLEDCRGBarray);
}
``` -->

#### GLSL functions
Its simple glsl like, not optimized or anything.
```
float clamp(float min, float max, float x);
float smoothstep(float edge0, float edge1, float x);
float fract(float f);
float dist(Vec2 a, Vec2 b);
float fmod(float a, float b);
Vec2 rotate(Vec2 v, float a);
```
